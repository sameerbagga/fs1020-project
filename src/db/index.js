'use strict';

let util = require('util');
let path = require('path');
let fs = require('fs');

let readFile = util.promisify(fs.readFile);
let writeFile = util.promisify(fs.writeFile);


let dbPath = path.resolve('src/db/phones.json');
let userPath = path.resolve('src/db/password.json');


/**
 * Reads and returns contents of db.json
 */
async function readPhones() {
  return readFile(dbPath)
    .then((contents) => {
      return JSON.parse(contents);
    });
}


/**
 * Replaces the contents of db.json
 */
async function writePhones(newPhone) {
  let json = JSON.stringify(newPhone, null, 2);
  return writeFile(dbPath, json);
}

// validating all of the required Phone information is entered
function validatePhoneInfo(newPhone) {
  // Validate name and brand is a non empty string
  if (!newPhone.name || !newPhone.brand || !newPhone.price || !newPhone.releaseYear || !newPhone.displaySize || !newPhone.ramGb || !newPhone.batterySize) {
    throw new Error('All fields must be entered with valid value.');
  }

  // Validate avalibility and smartphone is a boolean
  if (typeof newPhone.available !== 'boolean' || typeof newPhone.smartphone !== 'boolean') {
    throw new Error('Avalibility and Smartphone fields must be boolean only.');
  }

  // validating quantity and availibility are not contradicting each other
  if ((newPhone.available === false && newPhone.quantity) || (newPhone.available === true && (parseInt(newPhone.quantity)) === 0)) {
    throw new Error('Availability and Quantity are contradicting.');
  }
}

/**
 * Adds a new phone to db.json if it is valid
 */
async function createPhone(newPhone) {

  validatePhoneInfo(newPhone);

  return readPhones()
    .then((phones) => {
      console.log(phones);
      // Ensures name is unique
      phones.forEach((a) => {
        if (a.name === newPhone.name) {
          // function won't excute including not executing countries.push(country);
          throw new Error('Phone already exists.');
        }
      });

      phones.push(newPhone);
      return writePhones(phones);
    });
}

async function createUser(newUser) {
  //validate username
  if (!newUser.username || !newUser.password || typeof newUser.username !== 'string' || typeof newUser.password !== 'string') {
    throw new Error('Invalid username or password');
  }


  return readUsers()
    .then((user1) => {
      //check for unique username
      user1.forEach((inUser) => {
        if (inUser.username === newUser.username) {
          //function will not execute, and user will not be saved
          throw new Error('Username already in use');
        }
      });

      user1.push(newUser);
      return writeUsers(user1);
    });
}

async function readUsers() {
  return readFile(userPath)
    .then((contents) => {
    return JSON.parse(contents);
  });
}


async function usernameExists(username) {
  return readUsers()
    .then((users) => {
      let exists = false;

      users.forEach((user) => {
        if (user.username === username) {
          exists = true;
        }
      });

      return exists;
    });
}


function getUserPasswordHash(username) {
  return readUsers()
    .then((users) => {
      let match;

      users.forEach((user) => {
        if (user.username === username) {
          match = user;
        }
      });

      if (!match) {
        throw new Error('User does not exist.');
      }

      return match.password;
    });
}


async function writeUsers (newUser) {
  let userJson = JSON.stringify(newUser, null, 2);
  return writeFile(userPath, userJson);
}

async function updateProductById(id,updatedProduct) {
  return readPhones()
    .then((allPhones) => {
      return allPhones.map((phone) => {
        if (id > allPhones.length){
          throw new Error('Entered ID do not exist');
        } else if (phone.id === id) {
            validatePhoneInfo(updatedProduct);
            var newProduct = {
              id: phone.id,
              name: updatedProduct.name,
              price: updatedProduct.price,
              brand: updatedProduct.brand,
              releaseYear: updatedProduct.releaseYear,
              displaySize: updatedProduct.displaySize,
              ramGb: updatedProduct.ramGb,
              batterySize: updatedProduct.batterySize,
              quantity: updatedProduct.quantity,
              smartphone: updatedProduct.smartphone,
              available: updatedProduct.available
            }
          return newProduct;
        } else {
          return phone;
        }
      });
    })
    .then((products) => {
      return writePhones(products);
    });
}


async function deleteProductById(id) {
  return readPhones()
    .then((allPhones) => {
      if (id > allPhones.length){
        throw new Error('Entered ID do not exist');
      }
      return allPhones.filter((phone) => {
        if (phone.id !== id) {
          return phone;
        }
      });
    })
    .then((products) => {
      return writePhones(products);
    });
}

module.exports = {
  getAllPhones: readPhones,
  createPhone: createPhone,
  createUser: createUser,
  readUsers: readUsers,
  updateProductById: updateProductById,
  deleteProductById: deleteProductById,
  usernameExists: usernameExists,
  getUserPasswordHash: getUserPasswordHash,
};

'use strict';

let db = require('../db');

function getInventoryRoutes(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    db.getAllPhones()
      .then((phones) => {
        phones.forEach((phone) => {
          if (phone.smartphone) {
            phone.smartphone = 'true.png';
          } else {
            phone.smartphone = 'false.png';
          }

          if(phone.available) {
            phone.available = 'true.png';
          } else {
            phone.available = 'false.png';
          }
        });

        res.render('inventory', {
          pageId: 'inventory',
          title: 'Inventory',
          username: req.session.username,
          product: phones,
        });
      });
  }
}


module.exports = { get: getInventoryRoutes };

'use strict';

// let path = require('path');

// let userPath = path.resolve('src/db/password.json');
/**
 * Home page
 */
function getHomeRoute(req, res) {
  // userPath.getAllPhones()
  // .then((phone) => {
  res.render('home', {
    pageId: 'home',
    title: 'Home',
    username: req.session.username,
    // product: phone,
  });
  // });
}


module.exports = { get: getHomeRoute };

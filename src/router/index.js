'use strict';

let express = require('express');
let homeRoutes = require('./home');
let inventoryRoutes = require('./inventory');
let loginRoutes = require('./login');
let logoutRoutes = require('./logout');
let registerRoutes = require('./register');
let dbPhone = require('../db/phones')
let db = require('../db');

// Create instance of an express router
let router = express.Router();


/**
 * Define routes
 */

// Home page
router.get('/', homeRoutes.get);

// Inventory page
router.get('/inventory', inventoryRoutes.get);

// Login page
router.get('/login', loginRoutes.get);
router.post('/login', loginRoutes.post);

// Logout
router.get('/logout', logoutRoutes.get);

// Register page
router.get('/register', registerRoutes.get);
router.post('/register', registerRoutes.post);


// All commands for Postman
// list all product -- Postman

router.get('/product', (req, res, next) => {
  db.getAllPhones()
    .then((phone) => {
      res
      .status(200)
      .json(phone);
    })
    .catch (next);
});


//list product by ID or Name

router.get('/product/:input', (req, res,  next) => {
  try {
    const id = parseInt(req.params.input, 10);
    const inputName = req.params.input.toLowerCase();
    let matchedPhone;
    db.forEach((product) => {
      const actualName = product.name.toLowerCase();
      if (product.id === id || actualName === inputName) {
        matchedPhone = product;
      }
    });

    // Send matched product if that exists
    if (matchedPhone) {
      res
        .status(200)
        .json(matchedPhone);
    } else {
        res
          .status(404)
          .json({
            message: 'ID or Name match not found'
          });
      }
    } catch (error) {
        next (error);
      }
});


// add new product

router.post('/product', (req, res, next) => {

  let newPhone = {
    id: dbPhone.length + 1,
    name: req.body.name,
    price: req.body.price,
    brand: req.body.brand,
    releaseYear: req.body.releaseYear,
    displaySize: req.body.displaySize,
    ramGb: req.body.ramGb,
    batterySize: req.body.batterySize,
    quantity: req.body.quantity,
    smartphone: req.body.smartphone,
    available: req.body.available
  }

  console.log(newPhone);

  db.createPhone(newPhone)
  .then(() => {
    return res.status(200).json({
      success: 'true',
      message: 'phone added successfully',
      newPhone
    })
  })
  .catch (next);
});


// Update Phone by ID

router.put('/update/:id', (req,res,next) => {
  const inputId = parseInt(req.params.id, 10);
  db.updateProductById(inputId, req.body)
    .then(() => {
      res.redirect(200,'../product');
    })
    .catch(next);
});

// Delete Phone by ID

router.delete('/delete/:id', (req,res,next) => {
  const inputId = parseInt(req.params.id, 10);
  db.deleteProductById(inputId)
    .then(() => {
      res.redirect(200,'../product');
    })
    .catch(next);
});

module.exports = router;

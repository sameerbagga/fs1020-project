This Project is about a CellPhone Store.

Every CellPhone has following parameters:
-- id
-- name
-- price
-- brand
-- releaseYear
-- displaySize
-- ramGb
-- batterySize
-- quantity
-- smartphone
-- available

We can register or Login the Users.
Every user has only following 2x parameters:
-- username
-- password

Use Postman to run Part-1 of the project, and the following guide will help with the required commands.

Get Commands:
-- Get List of all CellPhones by using '/product'
-- Filter Cellphones by their id by using '/product/id'
-- Filter CellPhones by their name by using '/product/name'

Post Commands:
-- Post or add new Cellphone in database by using '/product'
-- Register any New User to database by using '/register'
-- Login any existing User by using '/login'

Put Command:
-- Update any existing cellphone information with its ID, using '/update/id'

Delete Command:
-- Delete any existing CellPhone with its ID, using '/delete/id'